from __future__ import print_function

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

import FtpClient
from user import *
import threading
# WATCH_PATH = r'C:\Users\Administrator\Desktop\dow'  # 监控目录

lock = threading.Lock
class FileMonitorHandler(FileSystemEventHandler):
    def __init__(self, path, port, **kwargs):
        super(FileMonitorHandler, self).__init__(**kwargs)
        # 监控目录
        self._watch_path = path
        self.port = port
    # 重写文件改变函数，文件改变都会触发文件夹变化
    # def on_modified(self, event):
    #
    #     # if not event.is_directory:  # 文件改变都会触发文件夹变化
    #     #     file_path = event.src_path
    #     #     print("文件改变: %s " % file_path)
    #
    # def on_created(self, event):
    #     print('创建了文件夹', event.src_path)
    #
    # def on_moved(self, event):
    #     print("移动了文件", event.src_path)
    #
    # def on_deleted(self, event):
    #     print("删除了文件", event.src_path)

    def on_any_event(self, event):
        lock.acquire()
        print(event.src_path, event.is_directory)
        user = User('admin', '123456')
        # user = User(request.cookies.get('username'), request.cookies.get('password'))
        ftp = FtpClient.ftpconnect("127.0.0.1", self.port, user.username, user.password)
        remote = '\\' + event.src_path.split('\\')[4]
        l = event.src_path.split('\\')
        local = l[0] + '\\' + l[1] + '\\' + l[2] + '\\' + l[3] + '\\' + l[4]
        print(remote)
        FtpClient.overwrite(ftp, local, remote)
        ftp.close()
        lock.release()

