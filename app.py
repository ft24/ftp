import os
from concurrent.futures.thread import ThreadPoolExecutor

from multiprocessing import Pool
from flask import Flask, request
import time

from flask_cors import cross_origin, CORS

from user import *
from JsUtils import *
from File import *
from WatchDog import *

import FtpClient
pool = ThreadPoolExecutor(max_workers=50)
app = Flask(__name__)

def creatWatchDog(path, port):
    event_handler = FileMonitorHandler(path=path, port=port)
    observer = Observer()
    observer.schedule(event_handler, path=path, recursive=True)  # recursive递归的
    observer.start()
    observer.join()

def getTime(path):
    list = os.listdir(path)
    filelist = []
    for file in list:
        filename = path + '\\' + file  # 当前路径
        t = os.path.getmtime(filename)
        timeStruct = time.localtime(t)
        ftime = time.strftime('%Y-%m-%d %H:%M:00', timeStruct)
        file = File(file, ftime, 5)
        filelist.append(file)
    return filelist

@cross_origin()
@app.route('/')
def hello_world():
    return 'Hello World!'

CORS(app, resources=r'/*')
@app.route('/userLogin', methods=['POST'])
def userLogin():
    data = request.data
    data = json.loads(data)
    user = User(data['username'],data['password'])
    js = Js()
    if user.juge() == True:
        js.data('code', 200)
    else:
        js.data('code', 500)
    return js.jsUtils()

CORS(app, resources=r'/*')
@app.route('/getLoacalFile', methods=['POST'])
def getLoacalFile():
    data = request.data
    data = json.loads(data)
    path = data['path']
    filelist = getTime(path)
    js = Js()
    js.data('filelist', filelist)
    return js.jsUtils()

CORS(app, resources=r'/*')
@app.route('/getGivenFileList', methods=['POST'])
def getGivenFileList():
    data = request.data
    data = json.loads(data)
    path = data['path']
    user = User('admin', '123456')
    # user = User(request.cookies.get('username'), request.cookies.get('password'))
    ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
    filelist = FtpClient.getFtpFileList(ftp, path)
    js = Js()
    js.data('flist', filelist)
    ftp.close()
    return js.jsUtils()

CORS(app, resources=r'/*')
@app.route('/creatDirectory', methods=['POST'])
def creatDirectory():
    data = request.data
    data = json.loads(data)
    path = data['path']
    port = data['port']
    f = pool.submit(creatWatchDog, path, port)
    js = Js()
    js.data('f',f.done())
    uploadGivenFiles(path)
    return js.jsUtils()

# @app.route('/uploadGivenFiles')
# @cross_origin()
def uploadGivenFiles(path):
    # data = request.data
    # data = json.loads(data)
    # path = data['path']
    # port = data['port']
    print(path)
    user = User('admin','123456')
    # user = User(request.cookies.get('username'), request.cookies.get('password'))
    ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
    ftp.mkd(path.split('\\')[-1])
    FtpClient.uploadDir(ftp, path, path.split('\\')[-1])
    ftp.close()
    js = Js()
    js.data('code',200)
    return js.jsUtils()

CORS(app, resources=r'/*')
@app.route('/loadGivenFiles', methods=['POST'])
def loadGivenFiles():
    data = request.data
    data = json.loads(data)
    path = data['path']
    port = data['port']
    filename ='\\' + data['fname']
    user = User('admin', '123456')
    # user = User(request.cookies.get('username'), request.cookies.get('password'))
    ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
    FtpClient.DownLoadFileTree(ftp, path, filename)
    ftp.close()
    path = path + filename
    f = pool.submit(creatWatchDog, path, port)
    js = Js()
    js.data('f', f.done())
    js.data('code',200)
    return js.jsUtils()


CORS(app, resources=r'/*')
@app.route('/deletFlies', methods=['POST'])
def deletFlies():
    data = request.data
    data = json.loads(data)
    path = '\\' + data['path']
    user = User('admin', '123456')
    # user = User(request.cookies.get('username'), request.cookies.get('password'))
    ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
    FtpClient.delAllfile(ftp, path)
    ftp.cwd('..')
    ftp.rmd(path)
# @app.route('/getPath')
# def getPath():
#     user = User(request.cookies.get('username'),request.cookies.get('password'))
#     ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
#     Path = FtpClient.getPwd(ftp)
#     ftp.close()
#     return Path

# @app.route('/getAllFileList')
# def getAllFileList():
#     user = User('admin','123456')
#     # user = User(request.cookies.get('username'),request.cookies.get('password'))
#     ftp = FtpClient.ftpconnect("127.0.0.1", 2021, user.username, user.password)
#     filelist = FtpClient.getFileList(ftp, '')
#     js = Js()
#     js.data('flist', filelist)
#     ftp.close()
#     return js.jsUtils()
CORS(app, resources=r'/*')
@app.route('/run', methods=['POST'])
def run():
    # host,port, username, password
    ftp = FtpClient.ftpconnect("127.0.0.1", 2021, "admin", "123456")
    # 下载文件，第一个是ftp服务器路径下的文件，第二个是要下载到本地的路径文件
    # FtpClient.downloadfile(ftp, "123.txt", r"C:\Users\Administrator\Desktop\dow\123.txt")
    # FtpClient.DownLoadFileTree(ftp, r"C:\Users\Administrator\Desktop\dow", "/123")
    # FtpClient.uploadDir(ftp, r"C:\Users\Administrator\Desktop\dow", '')
    # 上传文件，第一个是要上传到ftp服务器路径下的文件，第二个是本地要上传的的路径文件
    # FtpClient.uploadfile(ftp, '/upload/1.txt', "C:/Users/Administrator/Desktop/1.txt")
    # ftp.close() #关闭ftp
    # #调用本地播放器播放下载的视频
    # os.system('start D:\soft\kugou\KGMusic\KuGou.exe C:\Users\Administrator\Desktop\ftp\test.mp3')
    # print(ftp.getwelcome())  # 打印出欢迎信息
    # # 获取当前路径
    # pwd_path = ftp.pwd()
    # print("FTP当前路径:", pwd_path)
    # # # 显示目录下所有目录信息
    # # 设置FTP当前操作的路径
    # ftp.cwd('')
    # # 返回一个文件名列表
    # filename_list = ftp.nlst()
    # print(filename_list)
    #
    # ftp.mkd('目录名')  # 新建远程目录
    # ftp.rmd('目录名')  # 删除远程目录
    # ftp.delete('文件名')  # 删除远程文件
    # ftp.rename('fromname', 'toname')  # 将fromname修改名称为toname
    #
    # # 逐行读取ftp文本文件
    # file = '/upload/1.txt'
    # # ftp.retrlines('RETR %s' % file)
    # # 与 retrlines()类似，只是这个指令处理二进制文件。回调函数 cb 用于处理每一块（块大小默认为 8KB）下载的数据
    # # ftp.retrbinary('RETR %s' % file)
    ftp.close()
    return 'nice'
if __name__ == '__main__':
    app.run()
