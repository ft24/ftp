import json


class Js(object):
    js = {}

    def __init__(self):
        self.js = {}

    def data(self, dataname, data):
        self.js[dataname] = data

    def jsUtils(self):
        return json.dumps(self.js, default=lambda o: o.__dict__, sort_keys=True, indent=4, ensure_ascii=False)
