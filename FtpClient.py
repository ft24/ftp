# -*- coding: utf-8 -*-
from ftplib import FTP
import time,tarfile,os
from dateutil import parser
import sys
from File import *


#连接ftp
def ftpconnect(host,port, username, password):
    ftp = FTP()
    # 打开调试级别2，显示详细信息
    # ftp.set_debuglevel(2)
    ftp.connect(host, port)
    ftp.login(username, password)
    return ftp
#从ftp下载文件
def downloadfile(ftp, remotepath, localpath):
    # 设置的缓冲区大小
    bufsize = 1024
    fp = open(localpath, 'wb')
    ftp.retrbinary('RETR ' + remotepath, fp.write, bufsize)
    ftp.set_debuglevel(0)# 参数为0，关闭调试模式
    fp.close()
# 从ftp下载整个目录下的文件
def DownLoadFileTree(ftp, LocalDir, RemoteDir):
    print("remoteDir:", RemoteDir)
    if not os.path.exists(LocalDir):
        os.makedirs(LocalDir)
    ftp.cwd(RemoteDir)
    RemoteNames = ftp.nlst()
    print("RemoteNames1111", RemoteNames)
    for file in RemoteNames:
        Local = os.path.join(LocalDir, file)
        print(ftp.nlst(file),'2222')
        if file.find(".") == -1:
            if not os.path.exists(Local):
                os.makedirs(Local)
            DownLoadFileTree(ftp, Local, file)
        else:
            downloadfile(ftp, file, Local)
    ftp.cwd("..")
    return
#从本地上传文件到ftp
def uploadfile(ftp, remotepath, localpath):
    bufsize = 1024
    fp = open(localpath, 'rb')
    ftp.storbinary('STOR ' + remotepath, fp, bufsize)
    ftp.set_debuglevel(0)
    fp.close()
#从本地上传整个目录到ftp
def uploadDir(ftp, localdir, remotedir):
    ftp.cwd(remotedir)
    for file in os.listdir(localdir):
        src = os.path.join(localdir, file)
        if os.path.isfile(src):
            uploadfile(ftp, file, src)
        elif os.path.isdir(src):
            try:
                ftp.mkd(file)
            except:
                sys.stderr.write('the dir is exists %s' % file)
            uploadDir(ftp, src, file)
    ftp.cwd('..')

def getFtpFileList(ftp, path):
    # 设置FTP当前操作的路径
    ftp.cwd(path)
    # 返回一个文件名列表
    lines = []
    filelist = []
    # 设定空列表用于存储目录下信息
    print(ftp.dir())
    ftp.dir("", lines.append)
    # 通过ftp获取目录下信息
    fileinde = 0
    for line in lines:
    # 按照空字符分割文本
        fileinde = fileinde + 1
        tokens = line.split(maxsplit=9)
    # 获取文件名
        name = tokens[8]
    # 获取时间
        t = (int(tokens[7].split(':')[0])+8)%24
        if t > 10:
            tokens[7] = str(t) +  ':' + tokens[7].split(':')[1]
        else:
            tokens[7] = '0' + str(t) + ':' + tokens[7].split(':')[1]
        time_str = tokens[5] + " " + tokens[6] + " " + tokens[7]
        time = parser.parse(time_str)
        file = File(fileinde, name, str(time), 5)
        filelist.append(file)
    return filelist

def delAllfile(ftp,ftppath):
    print(ftppath)
    ftp.cwd(ftppath)
    print(ftp.nlst())
    if len(ftp.nlst()) <= 0:
        ftp.cwd('..')
        return True
    for i in ftp.nlst():
        print(i)
        if '.' in i:
            ftp.delete(i)
        else:
            delAllfile(ftp, ftp.pwd() + "/" + i)
            ftp.rmd(i)
    ftp.cwd('..')

def overwrite(ftp, local, remote):
    delAllfile(ftp, remote)
    uploadDir(ftp, local, remote)

def getPwd(ftp):
    pwd_path = ftp.pwd()
    return pwd_path