import time
from ftplib import FTP
import os


remote_path = "/"  # 远端目录
begin_time = 1500  # 任务开始时间
end_time = 1700  # 任务结束时间


today = time.strftime("%Y%m%d")  # 当天日期
today_file = today + 'test.txt'  # 得到当天日期的目标文件名
remote_file = remote_path + today_file  # 远端文件名
local_file = '\\\\local\\' + today + '\\' + today_file  # 本地文件名
log_file = 'C:\\\\log\\ftp_log.txt'


def ftp_connect():
    """用于FTP连接"""
    ftp_server = 'w.x.y.z'  # ftp站点对应的IP地址
    username = 'ftpuser'  # 用户名
    password = 'ftppass'  # 密码
    ftp = FTP()
    ftp.set_debuglevel(0) # 较高的级别方便排查问题
    ftp.connect(ftp_server, 21)
    ftp.login(username, password)
    return ftp

def remote_file_exists():
    """用于FTP站点目标文件存在检测"""
    ftp = ftp_connect()
    ftp.cwd(remote_path) # 进入目标目录
    remote_file_names = ftp.nlst()  # 获取文件列表
    ftp.quit()
    if today_file in remote_file_names:
        return True
    else:
        return False

def download_file():
    """用于目标文件下载"""
    ftp = ftp_connect()
    bufsize = 1024
    fp = open(local_file, 'wb')
    ftp.set_debuglevel(0) # 较高的级别方便排查问题
    ftp.retrbinary('RETR ' + remote_file, fp.write, bufsize)
    fp.close()
    ftp.quit()


while True:
    if int(time.strftime("%H%M")) in range(begin_time, end_time):  # 判断是否在执行时间范围
        if int(time.strftime("%Y%m%d")) - int(today) == 0:  # 判断是否跨日期
            while not os.path.exists(local_file):  # 判断本地是否已有文件
                if remote_file_exists():  # 判断远端是否已有文件
                    download_file()
                    with open(log_file, 'a') as f:
                        f.write('\n' + time.strftime("%Y/%m/%d %H:%M:%S") + " 今日文件已下载！")
                    time.sleep(60)  # 下载完毕静默1分钟
                else:
                    time.sleep(180)
                    break # 注意，此处跳出循环重新判断日期，避免周末或当天没文件时陷入内层循环
            else:
                time.sleep(180)
        else:
            """如果跨日期，则根据当前日期，更新各文件日期"""
            today = time.strftime("%Y%m%d")  # 当天日期
            today_file = today + 'test.txt'  # 得到当天日期的目标文件名
            remote_file = remote_path + today_file  # 远端文件名
            local_file = '\\\\local\\' + today + '\\' + today_file  # 本地文件名
            with open(log_file, 'a') as f:
                f.write('\n' + time.strftime("%Y/%m/%d %H:%M:%S") + " 任务启动, 文件日期已更新。")
    else:
        time.sleep(1800)